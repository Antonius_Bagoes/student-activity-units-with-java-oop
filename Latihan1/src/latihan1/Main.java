/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan1;

/**
 *
 * @author anton
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Membuat Beberapa Penduduk 
        Penduduk[] penduduk = new Penduduk[10];
        penduduk[0] = new MasyarakatSekitar("180", "Bambang", "1 Januari 1997");
        penduduk[1] = new MasyarakatSekitar("192", "Joko", "10 Maret 1998");
        penduduk[2] = new MasyarakatSekitar("105", "Supri", "16 Desember 1996");
        penduduk[3] = new MasyarakatSekitar("189", "Gino", "1 November 1994");
        penduduk[4] = new MasyarakatSekitar("156", "Tejo", "19 Oktober 1995");
        penduduk[5] = new Mahasiswa("185314100", "Supi", "25 Januari 2000");
        penduduk[6] = new Mahasiswa("185314152", "Tepho", "29 Oktober 2001");
        penduduk[7] = new Mahasiswa("185314111", "Widodo", "20 Maret 2000");
        penduduk[8] = new Mahasiswa("185314089", "Sukir", "28 September 2000");
        penduduk[9] = new Mahasiswa("185314008", "Kirman", "14 Mei 2002");

        //Membuat Beberapa UKM
        UKM ukm[] = new UKM[2];
        ukm[0] = new UKM("Tari");
        ukm[1] = new UKM("Paduan Suara");

        //MenSett Ketua dan Sekretaris Setiap UKM
        /*
            Pemisalan untuk Skenario ini 
            Ketua Tari = Tepho, Sekretaris = Supi.
            Ketua Paduan Suara = Kirman, Sekretaris = Sukir.
         */
        //UKM Tari
        ukm[0].setKetua((Mahasiswa) penduduk[6]);
        ukm[0].setSekretaris((Mahasiswa) penduduk[5]);

        //UKM Paduan Suara
        ukm[1].setKetua((Mahasiswa) penduduk[9]);
        ukm[1].setSekretaris((Mahasiswa) penduduk[8]);

        //Mensett Anggota yang tergabung di UKM
        /*
            Pemisalan untuk Skenario ini
            Angota Tari = Widodo, Bambang, Joko.
            Anggota PAduan Suara = Supri, Gino, Tejo.
         */
        Penduduk[] pendudukTemp;

        //UKM Tari
        pendudukTemp = new Penduduk[3];
        pendudukTemp[0] = penduduk[0];
        pendudukTemp[1] = penduduk[7];
        pendudukTemp[2] = penduduk[1];
        ukm[0].setAnggota(pendudukTemp);

        //UKM Paduan Suara
        pendudukTemp = new Penduduk[3];
        pendudukTemp[0] = penduduk[2];
        pendudukTemp[1] = penduduk[3];
        pendudukTemp[2] = penduduk[4];
        ukm[1].setAnggota(pendudukTemp);

        //Menampilkan UKM Beserta Isinya
        System.out.println("Daftar UKM Beserta Anggota-nya");
        for (int i = 0; i < ukm.length; i++) {
            System.out.println((i + 1) + ". UKM " + ukm[i].getNamaUnit());
            System.out.println("\tKetua      : " + ukm[i].getKetua().getNama());
            System.out.println("\tSekretaris : " + ukm[i].getSekretaris().getNama());
            System.out.println("\t---------------------------------------------");
            System.out.printf("%-5s", "\tNo");
            System.out.printf("%-15s", "Status");
            System.out.printf("%-15s", "Nama");
            System.out.printf("%-15s", "Nomor");
            System.out.println("");
            System.out.println("\t---------------------------------------------");
            Penduduk[] anggotaTemp = ukm[i].getAnggota();
            for (int j = 0; j < anggotaTemp.length; j++) {
                System.out.printf("%-5s", "\t" + (j + 1));
                if (anggotaTemp[j] instanceof Mahasiswa) {
                    System.out.printf("%-15s", "Mahasiswa");
                    System.out.printf("%-15s", ((Mahasiswa) anggotaTemp[j]).getNama());
                    System.out.printf("%-15s", ((Mahasiswa) anggotaTemp[j]).getNim());
                    System.out.println("");
                } else if (anggotaTemp[j] instanceof MasyarakatSekitar) {
                    System.out.printf("%-15s", "Masyarakat");
                    System.out.printf("%-15s", ((MasyarakatSekitar) anggotaTemp[j]).getNama());
                    System.out.printf("%-15s", ((MasyarakatSekitar) anggotaTemp[j]).getNomor());
                    System.out.println("");
                }
            }
            System.out.println("");
        }

        //Menampilkan Seluruh Penduduk
        System.out.println("");
        System.out.println("Daftar Penduduk");
        System.out.println("------------------------------------------------------------------------------------------------------");
        System.out.printf("%-5s", "No");
        System.out.printf("%-15s", "Nama");
        System.out.printf("%-20s", "Tanggal Lahir");
        System.out.printf("%-15s", "Status");
        System.out.printf("%-15s", "Nomor");
        System.out.printf("%-15s", "NIM");
        System.out.printf("%-15s", "Iuran");
        System.out.println("");
        System.out.println("------------------------------------------------------------------------------------------------------");
        for (int i = 0; i < penduduk.length; i++) {
            System.out.printf("%-5s", (i + 1));
            if (penduduk[i] instanceof Mahasiswa) {
                System.out.printf("%-15s", ((Mahasiswa) penduduk[i]).getNama());
                System.out.printf("%-20s", ((Mahasiswa) penduduk[i]).getTanggalLahir());
                System.out.printf("%-15s", "Mahasiswa");
                System.out.printf("%-15s", "-");
                System.out.printf("%-15s", ((Mahasiswa) penduduk[i]).getNim());
                System.out.printf("%-15s", "Rp. " + penduduk[i].hitungIuran());
                System.out.println("");
            } else if (penduduk[i] instanceof MasyarakatSekitar) {
                System.out.printf("%-15s", ((MasyarakatSekitar) penduduk[i]).getNama());
                System.out.printf("%-20s", ((MasyarakatSekitar) penduduk[i]).getTanggalLahir());
                System.out.printf("%-15s", "Masyarakat");
                System.out.printf("%-15s", ((MasyarakatSekitar) penduduk[i]).getNomor());
                System.out.printf("%-15s", "-");
                System.out.printf("%-15s", "Rp. " + penduduk[i].hitungIuran());
                System.out.println("");
            }
        }
        System.out.printf("%100s", "----------------");
        System.out.println("");
        double totalIuranTemp = 0;
        for (int i = 0; i < penduduk.length; i++) {
            totalIuranTemp += penduduk[i].hitungIuran();
        }
        System.out.printf("%101s", "Total " + "Rp. " + totalIuranTemp);
        System.out.println("");
    }

}
