/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan1;

/**
 *
 * @author anton
 */
public class Mahasiswa extends Penduduk {

    private String nim;

    public Mahasiswa(String nim, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nim = nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        return Double.parseDouble(nim) / 10000;
    }

}
