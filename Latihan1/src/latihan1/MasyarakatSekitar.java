/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan1;

/**
 *
 * @author anton
 */
public class MasyarakatSekitar extends Penduduk {

    private String nomor;

    public MasyarakatSekitar(String nomor, String nama, String tanggalLahir) {
        super(nama, tanggalLahir);
        this.nomor = nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        return Double.parseDouble(nomor.substring(0, 2))*100;
    }

}
